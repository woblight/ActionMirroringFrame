# ActionMirroringFrame

Addon for World of Warcraft (retail and classic)

## Description

It display the actions being performed (used action buttons) alongside their informations (hotkey, cooldown, usability, ...) in fixed point in the screen.

Although initially meant for streamer to allow easier understanding of their actions by the viewer, it may also result convenient for regular players to have a visual feedback on a fixed spot closer to the center of the screen.

## Installation

If you are downloading from gitlab, extract the folder contained into `Interface\AddOns` and rename it to ActionMirroringFrame. To check it's correctly installed be sure the `.toc` file is located at `Interface\AddOns\ActionMirroringFrame\ActionMirroringFrame.toc`

### Required Dependency

To work, ActionMirroringFrame requires [EmeralFramework](https://gitlab.com/woblight/EmeraldFramework) addon to be also installed.

## Usage

The position can be moved or resized and options can be viewed by sending the `/amf` chat command.

## Author

WobLight [Discord](https://discord.gg/WsVypM2)

## Thanks to

BimbzGG for testing
Lezonta for his great support and testing
